#### Installation guide

- Run docker containers: `docker-compose up -d`
- Enter inside docker container: `docker-compose exec app bash`
- Run composer install inside the container `composer install`
- Run migrations: `php bin/console doctrine:migrations:migrate`
- Populate DB with fixtures: `php bin/console doctrine:fixtures:load`
- App is available: `http://localhost:8080/`

#### Urls:
- **Product API** `/product`:
    - Product list `GET /product`
    - Single product `GET /product/1`
    - Add product `POST /product`. Expected request body:
  ```
  {
    "title": "Test title",
    "price": 15.99,
    "currency": "USD"
  }
  ```
    - Update product `PUT /product/1`. Expected request body, update either title or price for the specified currency:
  ```
  {
    "title": "Updated title",
    "price": 1.99,
    "currency": "USD
  }
  ```
    - Delete product `DELETE /product/1`
- **Cart API**
    - Create cart `POST /cart` with empty request body
    - Get cart `GET /cart/1` with its contents and total price
    - Put or remove products to cart `PUT /cart/1`. Expected request body:
  ```
  {
    "action": "add|remove",
    "product_id": 1,
    "quantity": 2
  }
  ```

#### Tests
Tests scenarios are stored in `/app/features`. In order to run tests execute:
```
docker-compose exec app bash
./vendor/bin/behat
```