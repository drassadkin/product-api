<?php

declare(strict_types=1);

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractRequest
{
    protected array $data;

    private ValidatorInterface $validator;

    public function __construct(Request $request)
    {
        $requestOptions = $this->resolveRequest($request);
        $requestResolver = new OptionsResolver();
        $this->validator = Validation::createValidator();
        $this->configureRequest($requestResolver);

        try {
            $this->data = $requestResolver->resolve($requestOptions);
        } catch (InvalidOptionsException | MissingOptionsException | UndefinedOptionsException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
    }

    /**
     * Convert request to array of options.
     */
    protected function resolveRequest(Request $request): array
    {
        $content = json_decode($request->getContent(), true) ?: [];
        $requestData = $request->request->all();

        return array_merge($requestData, $content);
    }

    /**
     * Configure resolver for the request.
     */
    public function configureRequest(OptionsResolver $resolver): void
    {
        $resolver->setRequired(static::required());
        $resolver->setDefined(static::optional());

        $this->configureAllowedValues($resolver);
        $this->configureRequestParametersTypes($resolver);

        $resolver->setDefaults(static::defaults());
    }

    /**
     * Should return an array of required request attributes.
     */
    abstract public static function required(): array;

    /**
     * Should return an array of optional request attributes.
     */
    abstract public static function optional(): array;

    /**
     * Should return an array with default values of request attributes.
     */
    abstract public static function defaults(): array;

    /**
     * Configure common request parameter types for application requests.
     */
    abstract protected function configureRequestParametersTypes(OptionsResolver $resolver): void;

    /**
     * Configure common values for application requests.
     */
    abstract protected function configureAllowedValues(OptionsResolver $resolver): void;

    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Validate option value with more complex constraints.
     */
    protected function validateOption($value, $constraints): void
    {
        $violations = $this->validator->validate($value, $constraints);

        if ($violations->has(0)) {
            throw new BadRequestHttpException($violations->get(0)->getMessage());
        }
    }
}