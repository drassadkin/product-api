<?php

declare(strict_types=1);

namespace App\Request;

use App\Service\CartService;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateCartRequest extends AbstractRequest
{

    public static function required(): array
    {
        return [
            'action',
            'product_id',
            'quantity',
        ];
    }

    public static function optional(): array
    {
        return [];
    }

    public static function defaults(): array
    {
        return [];
    }

    protected function configureRequestParametersTypes(OptionsResolver $resolver): void
    {
        $resolver->setAllowedTypes('action', 'string');
        $resolver->setAllowedTypes('product_id', 'int');
        $resolver->setAllowedTypes('quantity', 'int');
    }

    protected function configureAllowedValues(OptionsResolver $resolver): void
    {

        $resolver->setAllowedValues('action', [CartService::ACTION_ADD, CartService::ACTION_REMOVE]);

        $resolver->setAllowedValues('product_id', function ($value) {
            return $value > 0;
        });

        $resolver->setAllowedValues('quantity', function ($value) {
            return $value >= 1 && $value <= 10;
        });
    }
}