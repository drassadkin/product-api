<?php

declare(strict_types=1);

namespace App\Request;

use App\Service\PriceService;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateProductRequest extends AbstractRequest
{
    public static function required(): array
    {
        return [];
    }

    public static function optional(): array
    {
        return [
            'title',
            'price',
            'currency',
        ];
    }

    public static function defaults(): array
    {
        return [];
    }

    protected function configureRequestParametersTypes(OptionsResolver $resolver): void
    {
        $resolver->setAllowedTypes('title', 'string');
        $resolver->setAllowedTypes('price', 'float');
        $resolver->setAllowedTypes('currency', 'string');
    }

    protected function configureAllowedValues(OptionsResolver $resolver): void
    {

        $resolver->setAllowedValues('title', function ($value) {
            return $value !== '';
        });

        $resolver->setAllowedValues('price', function ($value) {
            return $value > 0 && $value < 100000;
        });

        $resolver->setAllowedValues('currency', PriceService::AVAILABLE_CURRENCIES);
    }
}