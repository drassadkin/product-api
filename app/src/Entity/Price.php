<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PriceRepository;
use Doctrine\ORM\Mapping as ORM;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;

/**
 * @ORM\Table(
 *     name="prices",
 *     uniqueConstraints={
 *        @ORM\UniqueConstraint(name="price_unique", columns={"product_id", "currency"})
 *    })
 * @ORM\Entity(repositoryClass=PriceRepository::class)
 */
class Price implements \JsonSerializable
{
    public const CURRENCY_USD = 'USD';
    public const CURRENCY_EUR = 'EUR';
    public const CURRENCY_PLN = 'PLN';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private string $amount;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private string $currency;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="price")
     * @ORM\JoinColumn(nullable=false)
     */
    private Product $product;

    private float $formattedAmount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'amount' => $this->formattedAmount,
            'currency' => $this->currency,
        ];
    }

    public function getFormattedAmount(): float
    {
        return $this->formattedAmount;
    }

    public function setFormattedAmount(float $formattedAmount): self
    {
        $this->formattedAmount = $formattedAmount;

        return $this;
    }

    public function setProduct(Product $product): Price
    {
        $this->product = $product;
        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}
