<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Table(name="carts")
 * @ORM\Entity(repositoryClass=CartRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Cart implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    private \DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true, name="modified_at")
     */
    private ?\DateTimeInterface $modifiedAt;

    /**
     * @ORM\OneToMany(targetEntity=CartEntry::class, mappedBy="cart", orphanRemoval=true)
     */
    private Collection $entries;

    private Price $totalPrice;

    public function __construct()
    {
        $this->entries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function touchModifiedAt(): self
    {
        $this->modifiedAt = new \DateTime('now', new \DateTimeZone('CET'));

        return $this;
    }

    /**
     * @return Collection|CartEntry[]
     */
    public function getEntries(): Collection
    {
        return $this->entries;
    }

    public function addEntry(CartEntry $entry): self
    {
        if (!$this->entries->contains($entry)) {
            $this->entries[] = $entry;
            $entry->setCart($this);
        }

        return $this;
    }

    public function removeEntry(CartEntry $entry): self
    {
        if ($this->entries->removeElement($entry)) {
            // set the owning side to null (unless already changed)
            if ($entry->getCart() === $this) {
                $entry->setCart(null);
            }
        }

        return $this;
    }

    public function setTotalPrice(Price $totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * @throws \Exception
     * @ORM\PrePersist()
     */
    public function beforeSave()
    {
        $this->createdAt = new \DateTime('now', new \DateTimeZone('CET'));
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'items' => $this->getEntries()->toArray(),
            'total' => $this->totalPrice
        ];
    }
}
