<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Cart;
use App\Entity\CartEntry;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CartFixtures extends Fixture implements DependentFixtureInterface
{
    private array $products;

    public function load(ObjectManager $manager)
    {
        $data = [
            '1' => [
                '1' => [
                    'cart_id' => 1,
                    'product_id' => 1,
                    'quantity' => 5,
                ],
                '2' => [
                    'cart_id' => 1,
                    'product_id' => 2,
                    'quantity' => 10,
                ],
                '3' => [
                    'cart_id' => 1,
                    'product_id' => 3,
                    'quantity' => 1,
                ],
            ],
            '2' => [
                '4' => [
                    'cart_id' => 2,
                    'product_id' => 1,
                    'quantity' => 1,
                ],
            ],
            '3' => [

            ],
        ];

        foreach ($data as $cartRow) {
            $cart = new Cart();

            foreach ($cartRow as $cartProductRow) {
                $cartEntry = new CartEntry();
                $cartEntry->setProduct($this->getProduct($manager, $cartProductRow['product_id']))
                    ->setCart($cart)
                    ->setQuantity($cartProductRow['quantity']);

                $manager->persist($cartEntry);
            }

            $manager->persist($cart);
        }

        $manager->flush();
    }

    private function getProduct(ObjectManager $manager, int $productId): ?Product
    {
        if (!isset($this->products)) {
            $products = $manager->getRepository(Product::class)->findAll();
            $this->products = $products;
        }

        return isset($this->products[$productId - 1]) ? $this->products[$productId - 1] : null;
    }

    public function getDependencies()
    {
        return [
            ProductFixtures::class,
        ];
    }
}
