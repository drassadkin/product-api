<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Price;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $data = [
            [
                'title' => 'Fallout',
                'price' => [
                    [
                        'amount' => '199',
                        'currency' => 'USD',
                    ],
                    [
                        'amount' => '189',
                        'currency' => 'EUR'
                    ]
                ],
            ],
            [
                'title' => 'Don\'t Starve',
                'price' => [
                    [
                        'amount' => '299',
                        'currency' => 'USD',
                    ]
                ],
            ],
            [
                'title' => 'Baldur\'s Gate',
                'price' => [
                    [
                        'amount' => '399',
                        'currency' => 'USD',
                    ]
                ],
            ],
            [
                'title' => 'Icewind Dale',
                'price' => [
                    [
                        'amount' => '499',
                        'currency' => 'USD',
                    ]
                ],
            ],
            [
                'title' => 'Bloodborne',
                'price' => [
                    [
                        'amount' => '599',
                        'currency' => 'USD',
                    ]
                ],
            ],
        ];

        foreach ($data as $productRow) {
            $product = new Product();
            $product->setTitle($productRow['title']);
            $manager->persist($product);
            foreach ($productRow['price'] as $priceRow) {
                $this->setPriceToProduct($manager, $product, $priceRow['amount'], $priceRow['currency']);
            }

        }

        $manager->flush();
    }

    private function setPriceToProduct(ObjectManager $manager, Product $product, string $amount, string $currency)
    {
        $manager->persist(
            (new Price())->setAmount($amount)->setCurrency($currency)->setProduct($product)
        );
    }
}
