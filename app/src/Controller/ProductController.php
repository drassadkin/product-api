<?php

declare(strict_types=1);

namespace App\Controller;

use App\Request\CreateProductRequest;
use App\Request\UpdateProductRequest;
use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    private ProductService $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @Route("/product", methods="GET", name="product_list")
     */
    public function getListAction(Request $request): Response
    {
        $page = $request->query->getInt('page');
        if ($page <= 0) {
            $page = 1;
        }

        $products = $this->productService->getProductList($page);
        [$totalRows, $totalPages] = $this->productService->getProductListPaginationInfo();

        return $this->json([
            'items' => $products,
            'page_num' => $page,
            'page_total' => $totalPages,
            'total_count' => $totalRows
        ]);
    }

    /**
     * @Route("/product/{productId<\d+>}", methods={"GET"}, name="product_single")
     */
    public function getAction(int $productId): JsonResponse
    {
        $product = $this->productService->getProduct($productId);
        if (!$product) {
            throw new NotFoundHttpException(sprintf('Product:%s not found', $productId));
        }

        return $this->json($product);
    }

    /**
     * @Route("/product", methods={"POST"}, name="product_create")
     */
    public function postAction(Request $request): JsonResponse
    {
        $createProductRequest = new CreateProductRequest($request);
        $product = $this->productService->createProduct($createProductRequest);

        return $this->json($product, 201, [
            'Location' => $this->generateUrl('product_single', ['productId' => $product->getId()])
        ]);
    }

    /**
     * @Route("/product/{productId<\d+>}", methods={"PUT"}, name="product_update")
     */
    public function putAction(int $productId, Request $request): JsonResponse
    {
        $updateProductRequest = new UpdateProductRequest($request);
        $product = $this->productService->updateProduct($productId, $updateProductRequest);

        if (!$product) {
            throw new NotFoundHttpException(sprintf('Product:%s not found', $productId));
        }

        return $this->json($product);
    }

    /**
     * @Route("/product/{productId<\d+>}", methods={"DELETE"}, name="product_delete")
     */
    public function deleteAction(int $productId): JsonResponse
    {
        $this->productService->deleteProduct($productId);
        return $this->json(null, 204);
    }
}
