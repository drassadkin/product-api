<?php

declare(strict_types=1);

namespace App\Controller;

use App\Request\UpdateCartRequest;
use App\Service\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    private CartService $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * @Route("/cart/{cartId<\d+>}", methods={"GET"}, name="cart")
     */
    public function getAction(int $cartId): JsonResponse
    {
        $cart = $this->cartService->getCart($cartId);
        if (!$cart) {
            throw new NotFoundHttpException(sprintf('Cart:%s not found', $cartId));
        }

        return $this->json($cart);
    }

    /**
     * @Route("/cart", methods={"POST"}, name="cart_create")
     */
    public function postAction(): JsonResponse
    {
        $cart = $this->cartService->createCart();

        return $this->json($cart, 201, [
            'Location' => $this->generateUrl('cart', ['cartId' => $cart->getId()])
        ]);
    }

    /**
     * @Route("/cart/{cartId<\d+>}", methods={"PUT"}, name="cart_update")
     */
    public function putAction(int $cartId, Request $request): JsonResponse
    {
        $updateCartRequest = new UpdateCartRequest($request);
        $cart = $this->cartService->updateCart($cartId, $updateCartRequest);

        if (!$cart) {
            throw new NotFoundHttpException(sprintf('Cart:%s not found', $cartId));
        }

        return $this->json([$cart]);
    }
}
