<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findAllPaginated(int $page = 1, int $limit = 10)
    {
        $firstResultIndex = ($page * $limit) - $limit;

        return $this->createQueryBuilder('p')
            ->where('p.deleted = false')
            ->setMaxResults($limit)
            ->setFirstResult($firstResultIndex)
            ->getQuery()
            ->getResult();
    }

    public function findAllTotalCount(): int
    {
        return (int)$this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findActive($id): ?Product
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :id')
            ->andWhere('p.deleted = false')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
