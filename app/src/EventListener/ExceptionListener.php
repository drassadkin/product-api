<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        $response = new JsonResponse();

        // Logic exception needs to be passed to logs only, it's not user's fault.
        if ($exception instanceof \LogicException) {
            $response->setData([
                'message' => 'Something went wrong',
                'debug' => $exception->getMessage()
            ]);
        } else {
            $response->setData([
                'message' => $exception->getMessage()
            ]);
        }

        $response->setStatusCode(
            $exception instanceof HttpExceptionInterface ?
                $exception->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR
        );

        $event->setResponse($response);
    }
}