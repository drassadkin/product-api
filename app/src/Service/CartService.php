<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Cart;
use App\Entity\CartEntry;
use App\Repository\CartRepository;
use App\Request\UpdateCartRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CartService
{
    public const ACTION_ADD = 'add';
    public const ACTION_REMOVE = 'remove';

    public const CART_PRODUCTS_LIMIT = 3;
    public const CART_COPIES_LIMIT = 10;

    private EntityManagerInterface $em;
    private CartRepository $repo;
    private PriceService $priceService;
    private ProductService $productService;

    public function __construct(
        EntityManagerInterface $entityManager,
        PriceService $priceService,
        ProductService $productService)
    {
        $this->em = $entityManager;
        $this->repo = $entityManager->getRepository(Cart::class);
        $this->priceService = $priceService;
        $this->productService = $productService;
    }

    public function getCart(int $cartId): ?Cart
    {
        $cart = $this->repo->find($cartId);
        $this->processCartPrice($cart);

        return $cart;
    }

    public function createCart(): Cart
    {
        $cart = $this->saveCart(new Cart());
        $this->processCartPrice($cart);

        return $cart;
    }

    public function updateCart(int $cartId, UpdateCartRequest $request): ?Cart
    {
        $cart = $this->repo->find($cartId);

        if ($cart === null) {
            return null;
        }

        $requestData = $request->getData();

        if ($requestData['action'] === self::ACTION_ADD) {
            $this->addToCart($cart, $requestData['product_id'], $requestData['quantity']);
        }
        if ($requestData['action'] === self::ACTION_REMOVE) {
            $this->removeFromCart($cart, $requestData['product_id'], $requestData['quantity']);
        }

        $cart->touchModifiedAt();
        $this->saveCart($cart);

        $this->processCartPrice($cart);

        return $cart;
    }

    private function addToCart(Cart $cart, int $productId, int $quantity): Cart
    {
        $isPerformed = false;

        foreach ($cart->getEntries() as $entry) {
            if ($entry->getProduct()->getId() !== $productId) {
                continue;
            }

            if ($entry->getQuantity() + $quantity > self::CART_COPIES_LIMIT) {
                throw new BadRequestHttpException(
                    sprintf('Limit of copies for Product:%s has reached', $productId)
                );
            }

            $entry->setQuantity($entry->getQuantity() + $quantity);
            $isPerformed = true;
            break;
        }

        // Product was not in the cart before - add new.
        if ($isPerformed === false) {
            if ($cart->getEntries()->count() >= self::CART_PRODUCTS_LIMIT) {
                throw new BadRequestHttpException('Limit of different products has reached');
            }

            $product = $this->productService->getProduct($productId);
            if ($product === null) {
                throw new NotFoundHttpException(sprintf('Product:%s not found', $productId));
            }

            $cartEntry = (new CartEntry())->setCart($cart)->setQuantity($quantity)->setProduct($product);
            $this->saveCartEntry($cartEntry);
            $cart->getEntries()->add($cartEntry);
        }

        return $cart;
    }

    private function removeFromCart(Cart $cart, int $productId, int $quantity): Cart
    {
        $isPerformed = false;
        foreach ($cart->getEntries() as $entry) {
            if ($entry->getProduct()->getId() !== $productId) {
                continue;
            }

            if ($entry->getQuantity() <= $quantity) {
                // Full remove of an entry.
                $cart->getEntries()->removeElement($entry);
            } else {
                $entry->setQuantity($entry->getQuantity() - $quantity);
            }
            $isPerformed = true;
            break;
        }

        if ($isPerformed === false) {
            throw new BadRequestHttpException(sprintf('Cart:%s does not have Product:%s', $cart->getId(), $productId));
        }

        return $cart;
    }

    private function processCartPrice(Cart $cart): Cart
    {
        /** @var CartEntry $entry */
        foreach ($cart->getEntries() as $entry) {
            $entry->getProduct()->setPrice(
                $this->priceService->getPriceForProduct($entry->getProduct())
            );
        }

        $cart->setTotalPrice($this->priceService->calculateTotalPriceOfCart($cart));

        return $cart;
    }

    private function saveCart(Cart $cart): Cart
    {
        $this->em->persist($cart);
        $this->em->flush();

        return $cart;
    }

    private function saveCartEntry(CartEntry $cartEntry): CartEntry
    {
        $this->em->persist($cartEntry);
        $this->em->flush();

        return $cartEntry;
    }
}