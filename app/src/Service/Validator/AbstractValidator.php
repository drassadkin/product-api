<?php

declare(strict_types=1);

namespace App\Service\Validator;

use App\Request\AbstractRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;

abstract class AbstractValidator
{
    protected AbstractRequest $request;
    protected ServiceEntityRepositoryInterface $repository;
    private array $params;

    public function __construct(AbstractRequest $request, ServiceEntityRepositoryInterface $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }

    public abstract function validate(): void;

    public function withParams(array $params): self
    {
        $this->params = $params;

        return $this;
    }

    protected function getParam(string $paramName)
    {
        if (!isset($this->params[$paramName])) {
            throw new \LogicException(sprintf('Param \'%s\' was not provided to %s', $paramName, static::class));
        }

        return $this->params[$paramName];
    }
}