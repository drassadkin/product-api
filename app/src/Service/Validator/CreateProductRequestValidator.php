<?php

declare(strict_types=1);

namespace App\Service\Validator;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class CreateProductRequestValidator extends AbstractValidator
{
    public function validate(): void
    {
        $requestData = $this->request->getData();

        $product = $this->repository->findOneBy(['title' => $requestData['title']]);

        if ($product !== null) {
            throw new BadRequestHttpException(
                sprintf('Title \'%s\' is already taken by Product:%s', $requestData['title'], $product->getId())
            );
        }
    }
}