<?php

declare(strict_types=1);

namespace App\Service\Validator;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class UpdateProductRequestValidator extends AbstractValidator
{
    public function validate(): void
    {
        $requestData = $this->request->getData();
        if (!isset($requestData['title'])) {
            return;
        }

        $product = $this->repository->findOneBy(['title' => $requestData['title']]);

        $productId = $this->getParam('productId');
        if ($product !== null && $product->getId() !== $productId) {
            throw new BadRequestHttpException(
                sprintf('Title \'%s\' is already taken by Product:%s', $requestData['title'], $product->getId())
            );
        }
    }
}