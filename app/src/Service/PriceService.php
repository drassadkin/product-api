<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Cart;
use App\Entity\Price;
use App\Entity\Product;
use App\Repository\PriceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;

class PriceService
{
    private const DEFAULT_CURRENCY = Price::CURRENCY_USD;

    public const AVAILABLE_CURRENCIES = [
        Price::CURRENCY_USD,
        Price::CURRENCY_EUR,
        Price::CURRENCY_PLN,
    ];

    private EntityManagerInterface $em;
    private PriceRepository $repo;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->repo = $entityManager->getRepository(Price::class);
    }

    public function getPriceForProduct(Product $product, string $currency = self::DEFAULT_CURRENCY): Price
    {
        $prices = $this->repo->findByProduct($product);

        if (count($prices) === 0) {
            throw new \LogicException(sprintf('Product:%s has no price assigned.', $product->getId()));
        }

        // Stores object with default currency for a case when the requested currency was not found.
        $defaultPrice = null;

        foreach ($prices as $price) {
            if ($price->getCurrency() === $currency) {
                return $this->formatPrice($price);
            }

            if ($price->getCurrency() === self::DEFAULT_CURRENCY) {
                $defaultPrice = $price;
            }
        }

        // If the price in default currency was not found => pick the first existing.
        /*
        *   TODO: given that a set of currencies is limited, it might be a good idea to implement
        *     currency prio (e.g. USD => EUR => PLN) instead of blindly picking the first available.
        */
        if ($defaultPrice === null) {
            $defaultPrice = $prices[0];
        }


        return $this->formatPrice($defaultPrice);
    }

    private function formatPrice(Price $price): Price
    {
        $currency = new Currency($price->getCurrency());
        $money = new Money($price->getAmount(), $currency);

        $price->setFormattedAmount(
            (float)(new DecimalMoneyFormatter(new ISOCurrencies()))->format($money)
        );

        return $price;
    }

    public function savePriceForProduct(Product $product, float $price, string $currency = self::DEFAULT_CURRENCY): Price
    {
        $price = floor($price * 100);

        $currency = new Currency($currency);
        $money = new Money($price, $currency);

        $price = $this->repo->findOneByProductAndCurrency($product, $currency->getCode());
        if ($price === null) {
            $price = (new Price())
                ->setProduct($product);
        }

        $price->setAmount($money->getAmount())
            ->setCurrency($currency->getCode());

        $price = $this->savePrice($price);

        $price->setFormattedAmount(
            (float)(new DecimalMoneyFormatter(new ISOCurrencies()))->format($money)
        );

        return $price;
    }

    public function calculateTotalPriceOfCart(Cart $cart): Price
    {
        // Ignore other currencies for simplicity, assume everything is USD.
        $total = new Money(0, new Currency(self::DEFAULT_CURRENCY));
        foreach ($cart->getEntries() as $entry) {
            $entryPrice = $entry->getProduct()->getPrice();
            $entryMoney = new Money($entryPrice->getAmount(), new Currency(self::DEFAULT_CURRENCY));
            $entryMoney = $entryMoney->multiply($entry->getQuantity());

            $total = $total->add($entryMoney);
        }

        $price = new Price();
        $price->setFormattedAmount(
            (float)(new DecimalMoneyFormatter(new ISOCurrencies()))->format($total)
        )->setCurrency(self::DEFAULT_CURRENCY);

        return $price;
    }

    private function savePrice(Price $price): Price
    {
        $this->em->persist($price);
        $this->em->flush();

        return $price;
    }
}