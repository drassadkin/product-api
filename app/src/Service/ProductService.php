<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Request\CreateProductRequest;
use App\Request\UpdateProductRequest;
use App\Service\Validator\CreateProductRequestValidator;
use App\Service\Validator\UpdateProductRequestValidator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductService
{
    private const LIST_COUNT_PER_PAGE = 3;

    private EntityManagerInterface $em;
    private ProductRepository $repo;
    private PriceService $priceService;

    public function __construct(EntityManagerInterface $entityManager, PriceService $priceService)
    {
        $this->em = $entityManager;
        $this->repo = $entityManager->getRepository(Product::class);
        $this->priceService = $priceService;
    }

    public function getProduct(int $id): ?Product
    {
        $product = $this->repo->findActive($id);

        if ($product === null) {
            return null;
        }

        $this->fetchProductPrice($product);

        return $product;
    }

    public function getProductList(int $page = 1): array
    {
        $products = $this->repo->findAllPaginated($page, self::LIST_COUNT_PER_PAGE);

        foreach ($products as $product) {
            $this->fetchProductPrice($product);
        }

        return $products;
    }

    public function getProductListPaginationInfo(): array
    {
        $totalRows = $this->repo->findAllTotalCount();
        return [$totalRows, (int)ceil($totalRows / self::LIST_COUNT_PER_PAGE)];
    }

    public function createProduct(CreateProductRequest $request): Product
    {
        $requestValidator = new CreateProductRequestValidator($request, $this->repo);
        $requestValidator->validate();

        $requestData = $request->getData();

        $product = $this->saveProduct(
            (new Product())->setTitle($requestData['title'])
        );

        $price = $this->priceService->savePriceForProduct($product, $requestData['price'], $requestData['currency']);
        $product->setPrice($price);

        return $product;
    }

    public function updateProduct(int $productId, UpdateProductRequest $request): ?Product
    {
        $requestValidator = new UpdateProductRequestValidator($request, $this->repo);
        $requestValidator->withParams(['productId' => $productId])->validate();

        $product = $this->repo->findActive($productId);

        if ($product === null) {
            return null;
        }

        $requestData = $request->getData();

        if (isset($requestData['title'])) {
            $product->setTitle($requestData['title'])
                ->touchModifiedAt();

            $this->saveProduct($product);
        }

        if (isset($requestData['price'])) {
            if (isset($requestData['currency'])) {
                $price = $this->priceService->savePriceForProduct($product, $requestData['price'], $requestData['currency']);
            } else {
                $price = $this->priceService->savePriceForProduct($product, $requestData['price']);
            }

            $product->setPrice($price);
        } else {
            $this->fetchProductPrice($product);
        }

        return $product;
    }

    public function deleteProduct(int $productId): void
    {
        $product = $this->repo->findActive($productId);

        if ($product === null) {
            return;
        }

        $product->setDeleted(true)
            ->touchDeletedAt()
            ->touchModifiedAt();

        $this->saveProduct($product);
    }

    private function saveProduct(Product $product): Product
    {
        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }

    private function fetchProductPrice(Product $product)
    {
        $price = $this->priceService->getPriceForProduct($product);
        $product->setPrice($price);
    }
}