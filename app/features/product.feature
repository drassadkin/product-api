Feature: Product API
  In order to use manage products
  As an API client
  I need to be able to list, add, update and remove products

  Background:
    Given the following products exist:
    | id | title         | price | currency |
    | 1  | Fallout       | 1.99  | USD      |
    | 2  | Don't starve  | 2.99  | USD      |
    | 3  | Baldur's Gate | 3.99  | USD      |
    | 4  | Icewind Dale  | 4.99  | USD      |
    | 5  | Bloodborne    | 5.99  | USD      |

  Scenario: Adding a new product
    Given the request body is:
      """
      {
        "title": "Fallout 2",
        "price": 6.99,
        "currency": "USD"
      }
      """
    When I request "/product" using HTTP POST
    Then the response code is 201
    And the "Location" response header is "/product/6"
    And the response body contains JSON:
      """
      {
        "id": 6
      }
      """

  Scenario: Adding a duplicated product
    Given the request body is:
      """
      {
        "title": "Fallout",
        "price": 1.99,
        "currency": "USD"
      }
      """
    When I request "/product" using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
      """
      {
        "message": "Title 'Fallout' is already taken by Product:1"
      }
      """

  Scenario: Getting product list
    When I request "/product" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "items": "@arrayLength(3)",
        "page_num": 1,
        "page_total": 2,
        "total_count": 5
      }
      """

  Scenario: Getting second page of product list
    When I request "/product?page=2" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "items": "@arrayLength(2)",
        "page_num": 2
      }
      """

  Scenario: Getting info about a specific product
    When I request "/product/1" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "id": 1,
        "title": "Fallout",
        "price": {
          "amount": 1.99,
          "currency": "USD"
        }
      }
      """

  Scenario: Getting info about non-existent product
    When I request "/product/15" using HTTP GET
    Then the response code is 404

  Scenario: Updating existing product's title
    Given the request body is:
      """
      {
        "title": "Fallout: Definitive edition"
      }
      """
    When I request "/product/1" using HTTP PUT
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "id": 1,
        "title": "Fallout: Definitive edition"
      }
      """

  Scenario: Updating existing product's price
    Given the request body is:
      """
      {
        "price": 15.99,
        "currency": "USD"
      }
      """
    When I request "/product/1" using HTTP PUT
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "id": 1,
        "price": {
          "amount": 15.99
        }
      }
      """

  Scenario: Updating existing product's title and price
    Given the request body is:
      """
      {
        "title": "Fallout: Definitive edition",
        "price": 15.99,
        "currency": "USD"
      }
      """
    When I request "/product/1" using HTTP PUT
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "id": 1,
        "title": "Fallout: Definitive edition",
        "price": {
          "amount": 15.99,
          "currency": "USD"
        }
      }
      """

  Scenario: Updating not-existing product's title
    Given the request body is:
      """
      {
        "title": "Fallout 2"
      }
      """
    When I request "/product/15" using HTTP PUT
    Then the response code is 404

  Scenario: Deleting a specific product
    When I request "/product/1" using HTTP DELETE
    Then the response code is 204
    And I request "/product/1" using HTTP GET
    And the response code is 404

  Scenario: Deleting a non-existing product
    When I request "/product/15" using HTTP DELETE
    Then the response code is 204