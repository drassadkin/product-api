Feature: Cart API
  In order to make purchases
  As an API client
  I need to be able to create cart, add products to the cart, remove products from the cart and see total price

  Background:
    Given the following products exist:
      | id | title         | price | currency |
      | 1  | Fallout       | 1.99  | USD      |
      | 2  | Don't starve  | 2.99  | USD      |
      | 3  | Baldur's Gate | 3.99  | USD      |
      | 4  | Icewind Dale  | 4.99  | USD      |
      | 5  | Bloodborne    | 5.99  | USD      |
    Given the following carts exist:
      | id |
      | 1  |
      | 2  |
      | 3  |
    Given the following cart entries exist:
      | id | cart_id | product_id | quantity |
      | 1  | 1       | 1          | 5        |
      | 2  | 1       | 2          | 10       |
      | 3  | 1       | 3          | 1        |
      | 4  | 2       | 1          | 1        |

  Scenario: Create a new cart
    When I request "/cart" using HTTP POST
    Then the response code is 201
    And the "Location" response header is "/cart/4"

  Scenario: Getting a cart info with products in it and total price
    When I request "/cart/1" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "id": 1,
        "items": [
          {
            "product": {
              "id": 1
            },
            "quantity": 5
          },
          {
            "product": {
              "id": 2
            },
            "quantity": 10
          },
          {
            "product": {
              "id": 3
            },
            "quantity": 1
          }
        ],
        "total": {
          "amount": 43.84,
          "currency": "USD"
        }
      }
      """

  Scenario: Getting an empty cart info
    When I request "/cart/3" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
    """
    {
      "id": 3,
      "items": [],
      "total": {
        "amount": 0,
        "currency": "USD"
      }
    }
    """

  Scenario: Adding a product into a cart
    Given the request body is:
      """
      {
        "action": "add",
        "product_id": 1,
        "quantity": 1
      }
      """
    When I request "/cart/3" using HTTP PUT
    Then the response code is 200
    And I request "/cart/3" using HTTP GET
    Then the response body contains JSON:
      """
      {
        "id": 3,
        "items": [
          {
            "product": {
              "id": 1
            },
            "quantity": 1
          }
        ],
        "total": {
          "amount": 1.99,
          "currency": "USD"
        }
      }
      """

  Scenario: Increasing quantity of a product in a cart
    Given the request body is:
      """
      {
        "action": "add",
        "product_id": 1,
        "quantity": 3
      }
      """
    When I request "/cart/1" using HTTP PUT
    Then the response code is 200
    And I request "/cart/1" using HTTP GET
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "items": [
          {
            "product": {
              "id": 1
            },
            "quantity": 8
          }
        ],
        "total": {
          "amount": 49.81,
          "currency": "USD"
        }
      }
      """

  Scenario: Decreasing quantity of a product in a cart
    Given the request body is:
      """
      {
        "action": "remove",
        "product_id": 1,
        "quantity": 3
      }
      """
    When I request "/cart/1" using HTTP PUT
    Then the response code is 200
    And I request "/cart/1" using HTTP GET
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "items": [
          {
            "product": {
              "id": 1
            },
            "quantity": 2
          }
        ],
        "total": {
          "amount": 37.87,
          "currency": "USD"
        }
      }
      """

  Scenario: Removing all copies of a product from a cart
    Given the request body is:
      """
      {
        "action": "remove",
        "product_id": 1,
        "quantity": 5
      }
      """
    When I request "/cart/1" using HTTP PUT
    Then the response code is 200
    And I request "/cart/1" using HTTP GET
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "total": {
          "amount": 33.89,
          "currency": "USD"
        }
      }
      """

  Scenario: Removing more copies of a product from a cart than cart has in it
    Given the request body is:
      """
      {
        "action": "remove",
        "product_id": 1,
        "quantity": 10
      }
      """
    When I request "/cart/1" using HTTP PUT
    Then the response code is 200
    And I request "/cart/1" using HTTP GET
    Then the response body contains JSON:
      """
      {
        "id": 1,
        "total": {
          "amount": 33.89,
          "currency": "USD"
        }
      }
      """

  Scenario: Increasing quantity of a product in a cart beyond 10
    Given the request body is:
      """
      {
        "action": "add",
        "product_id": 2,
        "quantity": 1
      }
      """
    When I request "/cart/1" using HTTP PUT
    Then the response code is 400
    And the response body contains JSON:
      """
      {
        "message": "Limit of copies for Product:2 has reached"
      }
      """

  Scenario: Adding fourth unique product to a cart
    Given the request body is:
      """
      {
        "action": "add",
        "product_id": 4,
        "quantity": 1
      }
      """
    When I request "/cart/1" using HTTP PUT
    Then the response code is 400
    And the response body contains JSON:
      """
      {
        "message": "Limit of different products has reached"
      }
      """