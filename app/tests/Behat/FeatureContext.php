<?php

declare(strict_types=1);

namespace App\Tests\Behat;

use App\DataFixtures\CartFixtures;
use App\DataFixtures\ProductFixtures;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\TableNode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Middleware;
use Imbo\BehatApiExtension\Context\ApiContext;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines application features from the specific context.
 */
final class FeatureContext extends ApiContext
{
    private ContainerInterface $container;
    private EntityManagerInterface $entityManager;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->container = $kernel->getContainer();
        $this->entityManager = $this->container->get('doctrine.orm.default_entity_manager');

        $schemaTool = new SchemaTool($this->entityManager);
        $schemaTool->dropDatabase();
        $metadata = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool->createSchema($metadata);
    }

    public function setClient(ClientInterface $client)
    {
        $stack = $client->getConfig('handler');
        $stack->push(Middleware::mapRequest(function (RequestInterface $request) {
            return $request->withAddedHeader('X-Behat', 'true');
        }));

        return parent::setClient($client);
    }

    /**
     * @BeforeScenario
     */
    public function loadDataFixtures(BeforeScenarioScope $scope)
    {
        $productFixtures = new ProductFixtures();
        $productFixtures->load($this->entityManager);

        $cartFixtures = new CartFixtures();
        $cartFixtures->load($this->entityManager);
    }

    /**
     * @Given the following products exist:
     */
    public function theFollowingProductsExist(TableNode $table)
    {
        // Stub. Adding is handled via fixtures in loadDataFixtures() method.
    }

    /**
     * @Given /^the following carts exist:$/
     */
    public function theFollowingCartsExist(TableNode $table)
    {
        // Stub. Adding test data is handled via fixtures in loadDataFixtures() method.
    }

    /**
     * @Given /^the following cart entries exist:$/
     */
    public function theFollowingCartEntriesExist(TableNode $table)
    {
        // Stub. Adding test data is handled via fixtures in loadDataFixtures() method.
    }
}
